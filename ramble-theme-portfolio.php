<?php
/**
 * @package Ramble Theme Portfolio
 * @version 1.0
 */
/*
Plugin Name: Ramble Theme Portfolio
Plugin URI: http://www.softhopper.net
Description: This plugin for ramble theme portfolio template
Author: SoftHopper
Author URI: http://softhopper.net
Version: 1.0
*/

function ramble_theme_custom_posts()
{

    $portfolio_label = array(
        'name' => esc_html_x('Portfolios', 'Post Type General Name', 'ramble'),
        'singular_name' => esc_html_x('Portfolio', 'Post Type Singular Name', 'ramble'),
        'menu_name' => esc_html__('Portfolios', 'ramble'),
        'parent_item_colon' => esc_html__('Parent Portfolio:', 'ramble'),
        'all_items' => esc_html__('All Portfolios', 'ramble'),
        'view_item' => esc_html__('View Portfolio', 'ramble'),
        'add_new_item' => esc_html__('Add New Portfolio', 'ramble'),
        'add_new' => esc_html__('New Portfolio', 'ramble'),
        'edit_item' => esc_html__('Edit Portfolio', 'ramble'),
        'update_item' => esc_html__('Update Portfolio', 'ramble'),
        'search_items' => esc_html__('Search Portfolios', 'ramble'),
        'not_found' => esc_html__('No portfolio found', 'ramble'),
        'not_found_in_trash' => esc_html__('No portfolios found in Trash', 'ramble'),
    );
    $portfolio_args = array(
        'label' => esc_html__('Portfolio', 'ramble'),
        'description' => esc_html__('Portfolio', 'ramble'),
        'labels' => $portfolio_label,
        'supports' => array('title'),
        'taxonomies' => array('portfolio-category'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-screenoptions',
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );
    register_post_type('portfolio', $portfolio_args);

    // Add new taxonomy, make it hierarchical (like categories) 

    $portfolio_taxonomy_labels = array(
        'name'              => esc_html__( 'Portfolio Categories','ramble' ),
        'singular_name'     => esc_html__( 'Portfolio Categories','ramble' ),
        'search_items'      => esc_html__( 'Search Portfolio Category','ramble' ),
        'all_items'         => esc_html__( 'All Portfolio Category','ramble' ),
        'parent_item'       => esc_html__( 'Parent Portfolio Category','ramble' ),
        'parent_item_colon' => esc_html__( 'Parent Portfolio Category:','ramble' ),
        'edit_item'         => esc_html__( 'Edit Portfolio Category','ramble' ),
        'update_item'       => esc_html__( 'Update Portfolio Category','ramble' ),
        'add_new_item'      => esc_html__( 'Add New Portfolio Category','ramble' ),
        'new_item_name'     => esc_html__( 'New Portfolio Category Name','ramble' ),
        'menu_name'         => esc_html__( 'Portfolio Category','ramble' ),
    );    

    // Now register the portfolio taxonomy
    register_taxonomy('portfolio', array('portfolio'), array(
        'hierarchical' => true,
        'labels' => $portfolio_taxonomy_labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'portfolio' ),
    ));
   
}

add_action('init', 'ramble_theme_custom_posts', 0);

/* CMB2 add meta field to portfolio */
add_action( 'cmb2_init', 'ramble_theme_register_portfolio_metabox' );
/**
 * Hook in and add a portfolio metabox. Can only happen on the 'cmb2_init' hook.
 */
function ramble_theme_register_portfolio_metabox() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_ramble_theme_';

    //portfolio metabox start
    $portfolio = new_cmb2_box( array(
        'id'            => $prefix . 'portfolio',
        'title'         => esc_html__( 'Portfolio Product Description', 'ramble' ),
        'object_types'  => array( 'portfolio', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $portfolio->add_field( array(
        'name' => esc_html__('Portfolio Description',"ramble"),
        'id' => $prefix . 'portfolio_description',
        'type' => 'textarea',
    ) );

    $portfolio->add_field( array(
        'name' => esc_html__('Portfolio Thumbnail',"ramble"),
        'desc' => esc_html__('Image size should 279x200',"ramble"),
        'id' => $prefix . 'portfolio_thumbnail',
        'type' => 'file',
    ) );

    $portfolio->add_field( array(
        'name' => esc_html__('Add single page images',"ramble"),
        'desc' => esc_html__('Image size should 650x450',"ramble"),
        'id' => $prefix . 'portfolio_images',
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ),
    ) );

    $portfolio->add_field( array(
        'name' => esc_html__('Date & Time',"ramble"),
        'id' => $prefix . 'date_time',
        'type' => 'text_medium',
    ) ); 

    $portfolio->add_field( array(
        'name' => esc_html__('Creator Name',"ramble"),
        'id' => $prefix . 'creator_name',
        'type' => 'text_medium',
    ) );
    $portfolio->add_field( array(
        'name' => esc_html__('Project Skills',"ramble"),
        'id' => $prefix . 'project_skills',
        'type' => 'text_medium',
    ) );  

    $portfolio->add_field( array(
        'name' => esc_html__('Spending Time',"ramble"),
        'id' => $prefix . 'spending_time',
        'type' => 'text_medium',
    ) ); 

    $portfolio->add_field( array(
        'name' => esc_html__('Project URL',"ramble"),
        'id' => $prefix . 'project_url',
        'type' => 'text_medium',
    ) );    
    //portfolio metabox end

}